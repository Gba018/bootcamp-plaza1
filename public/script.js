dragimg(document.getElementById("plaza1"));
dragimg(document.getElementById("plaza2"));
dragimg(document.getElementById("plaza3"));
dragimg(document.getElementById("plaza4"));
dragimg(document.getElementById("plaza5"));
dragimg(document.getElementById("plaza6"));
dragimg(document.getElementById("plaza7"));
dragimg(document.getElementById("plaza8"));
dragimg(document.getElementById("plaza9"));
dragimg(document.getElementById("plaza10"));
dragimg(document.getElementById("plaza11"));
dragimg(document.getElementById("plaza12"));
dragimg(document.getElementById("plaza13"));
dragimg(document.getElementById("plaza14"));
dragimg(document.getElementById("plaza15"));
dragimg(document.getElementById("plaza16"));
dragimg(document.getElementById("plaza17"));
dragimg(document.getElementById("plaza18"));

function dragimg(element) {
    element.onpointerdown = initDrag;
    let Dx = 0;
    let Dy = 0;

    function initDrag(e) {
        Dx = e.clientX - element.offsetLeft;
        Dy = e.clientY - element.offsetTop;
        document.onpointermove = dragElement;
        document.onpointerup = stopDrag;
    }

    function dragElement(e) {
        element.style.left = e.clientX - Dx + 'px';
        element.style.top = e.clientY - Dy + 'px';

    }

    function stopDrag(e) {
        document.onpointermove = null;
        document.onpointerup = null;
    }
}

